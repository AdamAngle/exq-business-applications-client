import './css/jquery.dataTables.css'
import './DataTable.css'
import React, { Component } from "react";

const $ = require('jquery');
$.DataTable = require('datatables.net');

require('bootstrap')

class Table extends Component {
  componentDidMount() {

    let ajax;
    if (this.props.ajax) {
      ajax = {
        'url': this.props.ajax,
        'type': 'PATCH'
      }
    }

    $(this.table).DataTable({
      dom: '<"data-table-wrapper"rtip>',
      data: this.props.data,
      ajax: ajax,
      columns: this.props.columns,
      columnDefs: this.props.columnDefs,
      ordering: true,
      serverSide: ajax ? true : false
    });
  }
  componentWillUnmount() {
    $('.data-table-wrapper')
      .find('table')
      .DataTable()
      .destroy(true);
  }
  shouldComponentUpdate() {
    return false;
  }
  render() {
    return (
      <div className="DataTable-wrapper">
        <table ref={a => this.table = a} />
      </div>);
  }
}

export default Table;
export { Table };