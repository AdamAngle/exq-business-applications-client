import React from 'react';
import './TopNavPage.css';
import { NavLink } from 'react-router-dom'
require('bootstrap')

class TopNavItem extends React.Component {
    _handleClick(menuItem) { 
      this.setState({ active: menuItem });
    }
  
    render() {
      return (
        <li className="nav-item">
          <NavLink to={this.props.item[1]}
            className="nav-link"
          >
            {this.props.item[0]}
          </NavLink>
        </li>
      )
    }
  }

function TopNav() {
    return (
        <div className="TopNav-main">
          <div className="container-fluid">
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
              <div className="collapse navbar-collapse" id="navbar_links">
                <ul className="navbar-nav">
                  {[].map((shItem, index) =>
                    <TopNavItem key={index} item={shItem} />
                  )}
                </ul>
              </div>
            </nav>
          </div>
        </div>
    );
}

class TopNavPage extends React.Component {
    render() {
        return (
            <div className="TopNavPage-main">
              <TopNav pages={[]} />
              <div className="container-fluid">
                {this.props.children}
              </div>
            </div>
        );
    }
}

export default TopNavPage;
export { TopNavPage };