import React, { Component } from "react";
import './Toolbar.css';

class Toolbar extends Component {

  render() {
    return (
      <div className="Toolbar-main">
          {this.props.children}
      </div>
    );
  }
}

class ToolbarSeparator extends Component {

  render() {
    return (
      <div className="Toolbar-separator"></div>
    );
  }
}

export default Toolbar;
export { Toolbar, ToolbarSeparator };