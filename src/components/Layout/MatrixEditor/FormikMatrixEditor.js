import React from "react";
const { useField } = require("formik");
const { default: MatrixEditor } = require("./MatrixEditor");

const FormikMatrixEditor = ({ columns, onChange, ...props }) => {
    const [field, meta, helpers] = useField(props);

    const { value } = meta;
    const { setValue } = helpers;

    const ChangeEventHandler = (e, data) => {
        setValue(data);
        if (onChange) onChange(e, data);
    }

    return (
        <div>
            <MatrixEditor data={value} columns={columns} onChange={(e, data) => ChangeEventHandler(e, data)} {...props} />
        </div>
    )
}

export default FormikMatrixEditor;
export {FormikMatrixEditor};
