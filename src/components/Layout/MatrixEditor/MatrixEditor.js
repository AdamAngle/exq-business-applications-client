import React, { useState } from "react";
import { Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ToolbarSeparator } from "../Toolbar/Toolbar";
import './MatrixEditor.css'

function MatrixEditor({columns=[], data=[], onChange, onRemoveRow, ...props}) {
  const [currentData, setCurrentData] = useState({data: data});
  //const [defaultData, setDefaultData] = useState({data: []});
  const [currentUniqueIndex, setCurrentUniqueIndex] = useState(-1);
  const [selectedRow, setSelectedRow] = useState(-1);

  var editor;

  const getEmptyRow = () => {
    var newIndex = currentUniqueIndex + 1;
    setCurrentUniqueIndex(newIndex);

    return {
      uniqueIndex: newIndex
    }
  }
  
  const AddRow = () => {
    setCurrentData({data: currentData.data.concat(getEmptyRow())});
  }

  const SelectRow = (rowUniqueIndex) => {
    setSelectedRow(rowUniqueIndex);
  }

  const RemoveRow = () => {
    if (selectedRow >= 0) {

      var newData = currentData.data.slice();
      newData.splice(selectedRow, 1)

      setCurrentData({data: newData});
      setSelectedRow(selectedRow > newData.length - 1 ? newData.length - 1 : selectedRow);
      if (onRemoveRow) onRemoveRow(newData);
    }
  }

  const EditCellData = (event, row, col) => {
    var newData = currentData.data.slice();
    newData[row][col] = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
    setCurrentData({data: newData});
    onChange(event, currentData.data);
  }

  const Serialize = () => {
    return currentData;
  }

  return (
    <>
      <table className="MatrixEditor-table" ref={a => editor = a}>
        <thead>
          
        </thead>
        <tbody>
          <tr className="MatrixEditor-head">
            <th className="selection-cell-header"></th>
            {columns.map((column, colindex) => {
              if (column.visible || !('visible' in column)) {
                return (
                  <th style={column.style} key={colindex}>
                    {column.title}
                  </th>
                )
              } else {
                return null;
              }
            })}
          </tr>
          {currentData.data.map((row, rowindex) => {
            return (
              <tr key={rowindex} onFocus={event => SelectRow(rowindex)}>
                <td className="selection-cell">
                  <input type="radio" checked={selectedRow === rowindex} readOnly={true} />
                  <div className="selected-row-color"></div>
                </td>
                {columns.map((column, colindex) => {
                  if (column.visible || !('visible' in column)) {
                    switch (column.editor) {
                      case "input":
                        return (
                          <td key={colindex}>
                            <input
                              type="text"
                              value={row[column.field] || column.default || ''}
                              onChange={event => EditCellData(event, rowindex, column.field)} />
                          </td>
                        )
                      case "numeric":
                        return (
                          <td key={colindex}>
                            <input
                              type="number"
                              value={row[column.field] || column.default || ''}
                              onChange={event => EditCellData(event, rowindex, column.field)} />
                          </td>
                        )
                      case "textarea":
                        return (
                          <td key={colindex}>
                            <textarea
                              rows="2"
                              value={row[column.field] || column.default || ''}
                              onChange={event => EditCellData(event, rowindex, column.field)} />
                          </td>
                        )
                      case "check":
                        return (
                          <td key={colindex}>
                            <input
                              type="checkbox"
                              checked={row[column.field] || column.default || false}
                              onChange={(event) => EditCellData(event, rowindex, column.field)} />
                          </td>
                        )
                      case "select":
                        return (
                          <td key={colindex}>
                            <select
                              value={row[column.field] || column.default || ''}
                              onChange={event => EditCellData(event, rowindex, column.field)}>
                              {column.options.map((option, optindex) => {
                                return (
                                  <option value={option.id} key={optindex}>
                                    {option.name}
                                  </option>
                                )
                                
                              })}
                            </select>
                          </td>
                        )
                      default:
                        return (
                          <td key={colindex}></td>
                        )
                    }
                  } else {
                    return null;
                  }
                })}
              </tr>
            )
          })}
        </tbody>
      </table>
      <div className="MultiTableEntry-toolbar">
        <Button variant="primary" size="sm" onClick={AddRow}><FontAwesomeIcon icon="check" size="sm" />&nbsp;Add</Button>
        <Button variant="light" size="sm"><FontAwesomeIcon icon="times" size="sm" />&nbsp;Cancel</Button>
        <ToolbarSeparator />
        <Button variant="light" size="sm" disabled={selectedRow === -1}>Copy Previous</Button>
        <Button variant="light" size="sm" disabled={selectedRow === -1}>Insert</Button>
        <Button variant="light" size="sm" onClick={event => RemoveRow()} disabled={selectedRow === -1}>Remove</Button>
        {props.toolbarAppend}
      </div>
    </>
  );
}

export default MatrixEditor;
export {MatrixEditor};