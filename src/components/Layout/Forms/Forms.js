import React, { Component } from "react";
import { Form, Col, Button, InputGroup, Tabs, Tab } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './Forms.css';
import './Tabulator.css';
import { Card } from 'react-bootstrap';
import { ToolbarSeparator } from "../Toolbar/Toolbar";
var Tabulator = require('tabulator-tables');

var get = require('lodash.get');

class FormText extends Component {

  render() {

    this.required = this.props.required;

    let requiredFlag;
    if (this.props.required) {
      requiredFlag = <span className="required-asterisk">*</span>
    }

    return (
      <Form.Group as={Col} lg="4" controlId={`form_input_${this.props.data_id}`}>
        <Form.Label>{ this.props.name }{ requiredFlag }</Form.Label>
        <span className="form-text-data">To Be Generated</span>
      </Form.Group>
    );
  }
}
/*
class FormSelect extends Component {

  render() {

    this.required = this.props.required;

    let requiredFlag;
    let requiredProp;
    if (this.props.required) {
      requiredFlag = <span className="required-asterisk">*</span>
      requiredProp = 'required'
    }

    return (
      <Form.Group as={Col} lg="4" controlId={`form_input_${this.props.data_id}`}>
        <Form.Label>{ this.props.name }{ requiredFlag }</Form.Label>
        <Form.Control as="select" type={ this.props.type } placeholder={ this.props.placeholder } size="sm" required={requiredProp} {...this.props}>
          {this.props.children}
        </Form.Control>
      </Form.Group>
    );
  }
}

class FormInput extends Component {

  render() {

    this.required = this.props.required;

    let requiredFlag;
    if (this.props.required) {
      requiredFlag = <span className="required-asterisk">*</span>
    }

    return (
      <Form.Group as={Col} lg="4" controlId={`form_input_${this.props.data_id}`}>
        <Form.Label>{ this.props.name }{ requiredFlag }</Form.Label>
        <Form.Control type={ this.props.type } placeholder={ this.props.placeholder } size="sm" />
      </Form.Group>
    );
  }
}
*/
class FormDialogListInput extends Component {

  render() {

    this.required = this.props.required;

    let requiredFlag;
    if (this.props.required) {
      requiredFlag = <span className="required-asterisk">*</span>
    }

    return (
      <Form.Group as={Col} lg="4" controlId={`form_input_${this.props.data_id}`} className="BuilderForm-dialogListInput">
        <Form.Label>{ this.props.name }{ requiredFlag }</Form.Label>
        <InputGroup size="sm">
          <div className="form-control form-control-dialog-list" ref={a => this.valueDiv = a}></div>
          <InputGroup.Append>
            <Button variant="outline-secondary" ref={a => this.dialogButton = a}>...</Button>
          </InputGroup.Append>
        </InputGroup>
        <Form.Control type="hidden" placeholder={ this.props.placeholder } size="sm" ref={a => this.input = a} />
      </Form.Group>
    );
  }
}

class FormGroupCard extends Component {

  render() {
    return (
      <Card className="BuilderForm-group">
          <Card.Header>{this.props.title}</Card.Header>
          <Card.Body>{this.props.children}</Card.Body>
      </Card>
    );
  }
}

class FormGroupTab extends Component {

  render() {
    return (
      <div className="BuilderForm-tabGroup">
          {this.props.children}
      </div>
    );
  }
}

class TabbedFormGroupCard extends Component {

  render() {

    // Parse the children objects as tabs.

    return (
      <Tabs defaultActiveKey={ this.props.defaultKey } {...this.props}>
      {React.Children.map(this.props.children, (tab, index) =>
        <Tab eventKey={ tab.props.tabKey } title={ tab.props.title }>
          {tab}
        </Tab>
      )}
      </Tabs>
    );
  }
}

class TabbedSubFormGroup extends Component {

  render() {

    // Parse the children objects as tabs.

    return (
      <Tabs defaultActiveKey={ this.props.defaultKey } {...this.props}>
      {React.Children.map(this.props.children, (tab, index) =>
        <Tab eventKey={ tab.props.tabKey } title={ tab.props.title }>
          {tab} 
        </Tab>
      )}
      </Tabs>
    );
  }
}

// Create an editable cell renderer
const EditableCell = ({
  value: initialValue,
  row: { index },
  column: { id },
  updateMyData, // This is a custom function that we supplied to our table instance
}) => {
  // We need to keep and update the state of the cell normally
  const [value, setValue] = React.useState(initialValue)

  const onChange = e => {
    setValue(e.target.value)
  }

  // We'll only update the external data when the input is blurred
  const onBlur = () => {
    updateMyData(index, id, value)
  }

  // If the initialValue is changed external, sync it up with our state
  React.useEffect(() => {
    setValue(initialValue)
  }, [initialValue])

  return <input type="text" value={value} onChange={onChange} onBlur={onBlur} />
}

export const getColumnWidth = (data, accessor, headerText) => {
  const cellLength = Math.max(
    ...data.map(row => {
      let value = '';

      if (typeof accessor === 'string') {
        value = get(row, accessor);
      } else {
        value = accessor(row);
      }

      if (typeof value === 'number') return value.toString().length;
      return (value || '').length;
    }),
    headerText.length
  );

  const magicSpacing = 10;
  return cellLength * magicSpacing;
};

// Set our editable cell renderer as the default Cell renderer
const defaultColumn = {
  Cell: EditableCell,
  width: 150
}

class MultiTableEntry extends Component {
  constructor(props) {
    super(props)
    this.columns = [
      {
        field: 'id',
        title: 'ID',
        visible: false,
        editor: 'input'
      }, {
        field: 'item_number',
        title: 'Item #',
        width: 150,
        editor: 'input'
      }, {
        field: 'item_url',
        title: 'Item URL',
        width: 165,
        editor: 'input'
      }, {
        field: 'description',
        title: 'Description',
        width: 270,
        editor: 'textarea'
      }, {
        field: 'quantity',
        title: 'Qty',
        headerAlign: 'center',
        width: 65,
        editor: 'numeric'
      }, {
        field: 'unit',
        title: 'UOM',
        headerAlign: 'center',
        width: 130,
        editor: 'select'
      }, {
        field: 'price',
        title: 'Pkg Price',
        width: 100,
        editor: 'numeric'
      }, {
        field: 'vendor_quote',
        title: 'Vendor Quote',
        editor: 'tickCross'
      }, {
        field: 'stock_checked',
        title: 'Stock Checked',
        editor: 'tickCross'
      }, {
        field: 'sds_required',
        title: 'SDS Required',
        editor: 'tickCross'
      }, {
        field: 'class',
        title: 'Class',
        headerAlign: 'center',
        width: 130,
        editor: 'select'
      }, {
        field: 'material_tracker',
        title: 'Material Tracker',
        editor: 'tickCross'
      }, {
        field: 'sensitive',
        title: 'Sensitive',
        headerAlign: 'center',
        editor: 'tickCross'
      }, {
        field: 'federal_supply_code',
        title: 'Federal Supply Code',
        width: 200,
        editor: 'select'
      },
    ];
  }

  componentDidMount() {

    // Initialize the Tabulator table
    this.table = new Tabulator(this.tableDiv, {
      data:[{}], //assign data to table
      layout:"fitColumns", //fit columns to width of table (optional)
      columns: this.columns,
      initialSort: [
        {column: 'id', dir: 'asc'}
      ],
      headerSort: false
    });
  }

  render() {
    return (
      <>
        <div ref={t => this.tableDiv = t} />
        <div className="MultiTableEntry-toolbar">
          <Button variant="primary" size="sm"><FontAwesomeIcon icon="check" size="sm" />&nbsp;Add</Button>
          <Button variant="light" size="sm"><FontAwesomeIcon icon="times" size="sm" />&nbsp;Cancel</Button>
          <ToolbarSeparator />
          <Button variant="light" size="sm">Copy Previous</Button>
          <Button variant="light" size="sm">Insert</Button>
          <Button variant="light" size="sm">Remove</Button>
        </div>
      </>
    )
  }
}

export { FormText, FormGroupCard, FormGroupTab, TabbedFormGroupCard, TabbedSubFormGroup, FormDialogListInput, MultiTableEntry };
