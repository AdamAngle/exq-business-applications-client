import React, { useState } from 'react';
import { Form, Col, Button, InputGroup } from 'react-bootstrap';
import { useField } from 'formik';
import ListInputDialog from '../Dialogs/ListInputDialog';

export const FormDialogListInput = ({ setDisplay, label, ...props }) => {

  const [field, meta, helpers] = useField(props);

  const [show, setShow] = useState(false);
  const [displayValue, setDisplayValue] = useState('');

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const { value } = meta;
  const { setValue } = helpers;

  var dialog;
  var valueDiv;
  var dialogButton;
  var input;

  const handleSet = (row) => {
    console.log(row);
    setValue(row.id);
    setDisplayValue(setDisplay ? setDisplay(row) : '');
    handleClose();
  }

  return (
    <>
      <ListInputDialog
        show={show}
        title={props.title}
        columns={props.columns}
        ajax={props.ajax}
        onSet={(row) => handleSet(row)}
        onHide={() => handleClose()} />
      <Form.Group
        as={Col}
        lg="4"
        controlId={`form_input_${field.name}`}
        className="BuilderForm-dialogListInput"
      >
        <Form.Label>
          { label }
          { props.required ? <span className="required-asterisk">*</span> : null }
        </Form.Label>
        <InputGroup size="sm">
          <div className="form-control form-control-dialog-list" ref={a => valueDiv = a}>{ displayValue }</div>
          <InputGroup.Append>
            <Button variant="outline-secondary" ref={a => dialogButton = a} onClick={() => handleShow()}>...</Button>
          </InputGroup.Append>
        </InputGroup>
        <Form.Control type="hidden" size="sm" ref={a => input = a} {...props} />
      </Form.Group>
    </>
    
  );
}