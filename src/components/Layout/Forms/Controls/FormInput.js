import React, { Component } from 'react';
import { Form, Col } from 'react-bootstrap';
import { useField } from 'formik';

export const FormInput = ({ label, ...props }) => {

  const [field, meta, helpers] = useField(props);

  return (
    <Form.Group as={Col} lg="4" controlId={`form_input_${field.name}`}>
      <Form.Label>
        { label }
      </Form.Label>
      <Form.Control {...field} {...props} size="sm" />
    </Form.Group>
  );
}