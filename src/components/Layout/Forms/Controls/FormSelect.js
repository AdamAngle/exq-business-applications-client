import React from 'react';
import { Form, Col } from 'react-bootstrap';
import { useField } from 'formik';

export const FormSelect = ({ label, ...props }) => {

  const [field, meta, helpers] = useField(props);

  return (
    <Form.Group as={Col} lg="4" controlId={`form_input_${field.name}`}>
      <Form.Label>
        { label }
        { props.required ? (<span className="required-asterisk">*</span>) : null }
      </Form.Label>
      <Form.Control
        as="select"
        onChange={(evt) => helpers.setValue(evt.target.value)}
        onBlur={() => helpers.setTouched(true)}
        size="sm"
        {...props} />
    </Form.Group>
  );
}
