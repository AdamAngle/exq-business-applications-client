import React, { useEffect, useState } from 'react';
import { Form, Col } from 'react-bootstrap';
import { useField } from 'formik';
import { FormSelect } from './FormSelect';


const useFetch = (url, existingData) => {
  const [data, setData] = useState(null);

  // empty array as second argument equivalent to componentDidMount
  useEffect(() => {
    async function fetchData() {
      if (existingData === null) {
        const response = await fetch(url);
        const json = await response.json();
        setData(json);
      } else {
        setData(existingData);
      }
      console.log(data)
    }
    fetchData();
  }, [url]);

  return data;
};

export const FormContractSelectGroup = ({ jobCodeName, taskNumberName, ...props }) => {

  const [field, meta, helpers] = useField(props);

  const contracts = useFetch('http://localhost:5000/api/general/contracts', null);

  return (
    <>
      <FormSelect name={jobCodeName} label="Job Code" type="text">
        
      </FormSelect>
      <FormSelect name={taskNumberName} label="Task Number" type="text"></FormSelect>
    </>
  );
}
