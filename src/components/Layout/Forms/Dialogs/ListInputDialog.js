import Table from 'components/Layout/DataTable/DataTable';
import React from 'react';
import {render} from 'react-dom';
import { Modal, Button } from 'react-bootstrap';

// See https://stackoverflow.com/a/6195753/5685076
function prependArray(value, array) {
  if (array) {
    var newArray = array.slice();
    newArray.unshift(value);
    return newArray;
  } else {
    return [value];
  }
}

const ListInputDialog = ({onSet, ...props}) => {

  const cols = prependArray({
    title: '',
    orderable: false,
    data: null,
  }, props.columns)

  const colDefs = [
    {
      targets: [0],
      createdCell: (td, cell, row) => {
        render(
          <Button onClick={(e) => onSet ? onSet(row) : false} variant="info" size="sm">
            Set
          </Button>,
          td
        )
      }
    }
  ]

  return (
    <Modal
      {...props}
      aria-labelledby="contained-modal-title-vcenter"
      dialogClassName="modal-90w"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          {props.title}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Table columns={cols} ajax={props.ajax} columnDefs={colDefs}/>
      </Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default ListInputDialog;
export {ListInputDialog};