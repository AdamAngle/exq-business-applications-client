import React from 'react';
require('bootstrap')

class Card extends React.Component {
  render() {
    return (
      <div className="card mb-4">
        <div className="card-header">
          {this.props.title}
        </div>
        {this.props.children}
      </div>
    )
  }
}

class CardBody extends React.Component {
  render() {
    return (
      <div {...this.props} className={`card-body ${this.props.className}`}>
        {this.props.children}
      </div>
    )
  }
}

export default Card;
export { Card, CardBody };