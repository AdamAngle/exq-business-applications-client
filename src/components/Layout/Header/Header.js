import React from 'react';
import './Header.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { NavLink } from 'react-router-dom'
require('bootstrap')

function HeaderSearchBar() {
    return (
        <div className="HeaderSearchBar-main">
            <div className="p-1 bg-light rounded rounded-pill shadow-focus">
                <div className="input-group">
                <input type="search" placeholder="Search for anything..." aria-describedby="button-addon1" className="form-control border-0 bg-light" />
                <div className="input-group-append">
                    <button id="button-addon1" type="submit" className="btn btn-link text-primary"><FontAwesomeIcon icon="search" /></button>
                </div>
                </div>
            </div>
        </div>
    );
}

function HeaderProfile() {
  return (
    <div className="HeaderProfile-main">
      <img className="circle HeaderProfile-thumb" alt="Profile" src={require('./profile.jpg')} />
      <div className="HeaderProfile-layerText">
        <b>Adam Angle</b>
        <span>EXQ Business Apps - Administrator</span>
      </div>
    </div> 
  );
}

const subheaderItems = [
  [ <FontAwesomeIcon icon="history" size="lg" />, '/History/' ],
  [ <FontAwesomeIcon icon="home" size="lg" />, '/' ],
  [ 'Requisitions', '/Requisitions/' ],
  [ 'Vendors', '/Vendors/' ],
  [ 'Shipping', '/Shipping/' ],
  [ 'Receiving', '/Receiving/' ],
  [ 'Reports', '/Reports/' ],
  [ 'Documents', '/Documents/' ]
]

class SubHeaderItem extends React.Component {
  _handleClick(menuItem) { 
    this.setState({ active: menuItem });
  }

  render() {
    var navclass = "nav-item";
    if (this.props.index === 0) {
      navclass = "nav-item nav-item-first";
    }
    return (
      <li className={navclass}>
        <NavLink exact to={this.props.item[1]}
          className="nav-link"
        >
          {this.props.item[0]}
        </NavLink>
      </li>
    )
  }
}

class SubHeader extends React.Component {

  render() {
    return (
        <div className="SubHeader-main">
          <div className="container-fluid">
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
              <div className="collapse navbar-collapse" id="navbar_links">
                <ul className="navbar-nav">
                  {subheaderItems.map((shItem, index) =>
                    <SubHeaderItem key={index} index={index} item={shItem} />
                  )}
                </ul>
              </div>
            </nav>
          </div>
        </div>
    )
  }
}


function Header() {
    return (
      <div className="Header-main">
        <div className="container-fluid">
          <div className="Header-left">
            <img className="HeaderProfile-thumb" alt="Logo" /*src={require('./logo.svg')}*/ />
          </div>
          <div className="Header-center">
            <HeaderSearchBar />
          </div>
          <div className="Header-right">
            <FontAwesomeIcon icon={['far', 'bell']} size="lg" />
            <HeaderProfile user="Adam Angle" app="Administrator" />
          </div>
        </div>
      </div>
    );
}

export default Header;
export { Header, SubHeader };