import React, { Component } from "react";
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { Card, CardBody } from "../../Layout/Card/Card.js"
import MainLayout from "components/Pages/MainLayout.js"
 
class Home extends Component {
  render() {
    return (
      <MainLayout title="Home">
        <div className="row">
          <div className="col-lg-3">
            <Card title="Quick Links" className="mb-2">
              <ul className="list-group list-group-flush">
                <li className="list-group-item">Cras justo odio</li>
                <li className="list-group-item">Dapibus ac facilisis in</li>
                <li className="list-group-item">Vestibulum at eros</li>
              </ul>
            </Card>
          </div>
          <div className="col-lg-9">
            <Card title="Reminders">
              <CardBody className="bg-warning-light">
                <h6>No content available</h6>
                <span>This panel has not been set up by your administrator.</span>
              </CardBody>
            </Card>
            <Card title="Purchases">
              <CardBody >
                Content
              </CardBody>
            </Card>
          </div>
        </div>
      </MainLayout>
        
    );
  }
}

export {Home};
export default Home;