import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Row, Button, ButtonGroup, Dropdown } from 'react-bootstrap';
import { Form as FormikForm, Formik, FormikProps } from 'formik';
import MainLayout from 'components/Pages/MainLayout.js'
import Toolbar, { ToolbarSeparator } from 'components/Layout/Toolbar/Toolbar.js'
import {FormGroupCard, FormText, FormGroupTab, TabbedFormGroupCard, TabbedSubFormGroup} from 'components/Layout/Forms/Forms.js'
import {FormInput} from 'components/Layout/Forms/Controls/FormInput';
import {FormSelect} from 'components/Layout/Forms/Controls/FormSelect';
import {FormDialogListInput} from 'components/Layout/Forms/Controls/FormDialogListInput';
import { FormikMatrixEditor } from 'components/Layout/MatrixEditor/FormikMatrixEditor.js';
import { isArray } from "jquery";
import { useToasts, withToastManager } from "react-toast-notifications";
import { FormContractSelectGroup } from "components/Layout/Forms/Controls/FormContractSelectGroup";
require("animate.css");
 
class AddRequisition extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      formGroupTabTitles: {},
      contracts: [],
      contractTasks: []
    };

    this.toastManager = props.toastManager;

    this.handleInputChange = this.handleInputChange.bind(this);
    this.ChangeFormGroupTabPriceTitle = this.ChangeFormGroupTabPriceTitle.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleApiSubmission = this.handleApiSubmission.bind(this);
    this.onSubmitError = this.onSubmitError.bind(this);
    this.onSubmitSuccess = this.onSubmitSuccess.bind(this);

    // Retrieve any columns for the MultiTableEntry controls
    this.Requisitions_matrixColumns_items = [
      {
        field: 'id',
        title: 'ID',
        visible: false,
        editor: 'input'
      }, {
        field: 'item_number',
        title: 'Item #',
        style: {minWidth: '150px'},
        editor: 'input'
      }, {
        field: 'item_url',
        title: 'Item URL',
        style: {minWidth: '165px'},
        editor: 'input'
      }, {
        field: 'description',
        title: 'Description',
        style: {minWidth: '270px'},
        editor: 'textarea'
      }, {
        field: 'quantity',
        title: 'Qty',
        headerAlign: 'center',
        style: {minWidth: '65px'},
        editor: 'numeric'
      }, {
        field: 'unit',
        title: 'UOM',
        headerAlign: 'center',
        style: {minWidth: '130px'},
        editor: 'select',
        options: []
      }, {
        field: 'price',
        title: 'Pkg Price',
        style: {minWidth: '100px'},
        editor: 'numeric'
      }, {
        field: 'vendor_quote',
        title: 'Vendor Quote',
        style: {textAlign: 'center'},
        editor: 'check'
      }, {
        field: 'stock_checked',
        title: 'Stock Checked',
        style: {textAlign: 'center'},
        editor: 'check'
      }, {
        field: 'sds_required',
        title: 'SDS Required',
        style: {textAlign: 'center'},
        editor: 'check'
      }, {
        field: 'class',
        title: 'Class',
        headerAlign: 'center',
        style: {minWidth: '130px'},
        editor: 'select',
        options: []
      }, {
        field: 'material_tracker',
        title: 'Material Tracker',
        style: {textAlign: 'center'},
        editor: 'check'
      }, {
        field: 'sensitive',
        title: 'Sensitive',
        headerAlign: 'center',
        editor: 'select',
        options: [
          {name: ""},
          {id: 0, name: "No"},
          {id: 1, name: "Yes"}
        ]
      }, {
        field: 'federal_supply_code',
        title: 'Federal Supply Code',
        style: {minWidth: '200px'},
        editor: 'select',
        options: []
      },
    ];

    this.Requisitions_matrixColumns_expenses = [
      {
        field: 'id',
        title: 'ID',
        visible: false,
        editor: 'input'
      }, {
        field: 'category',
        title: 'Category',
        editor: 'select',
        options: [
          {name: 'Uncategorized'}, // This will be replaced by a server call for categories
          {id: 1, name: 'Tax'}
        ]
      }, {
        field: 'amount',
        title: 'Amount',
        editor: 'numeric'
      }, {
        field: 'memo',
        title: 'Memo',
        editor: 'textarea'
      }
    ]

    this.Requisitions_matrixColumns_contacts = [
      {
        field: 'id',
        title: 'ID',
        visible: false,
        editor: 'input'
      }, {
        field: 'contact',
        title: 'Contact',
        editor: 'input'
      }, {
        field: 'tob_title',
        title: 'Job Title',
        editor: 'input'
      }, {
        field: 'email',
        title: 'Email Address',
        editor: 'input'
      }, {
        field: 'main_phone',
        title: 'Main Phone',
        editor: 'input'
      }, {
        field: 'subsidiary',
        title: 'Subsidiary',
        editor: 'select',
        options: [
          {name: ""},
          {id: 0, name: "Parent Company"}
        ]
      }, {
        field: 'role',
        title: 'Role',
        editor: 'select',
        options: [
          {name: ""},
          {id: 0, name: "Primary Contact"}
        ]
      }
    ]

    this.Requisitions_dlgListInput_Requester = [
      {
        title: 'First Name',
        data: 'first_name'
      },
      {
        title: 'Last Name',
        data: 'last_name'
      },
      {
        title: 'Username',
        data: 'username'
      },
      {
        title: 'Email Address',
        data: 'email'
      }
    ]

    this.Requisitions_dlgListInput_Vendor = [
      {
        title: 'Vendor Name',
        data: 'name'
      },
      {
        title: 'Vendor Phone',
        data: 'phone'
      },
      {
        title: 'Vendor Email',
        data: 'email'
      },
      {
        title: 'Vendor Contact',
        data: 'contact'
      },
      {
        title: 'Website',
        data: 'website'
      }
    ]
  }

  componentDidMount() {

    // Get list of contracts
    const url = 'http://localhost:5000/api/general/contracts'
    
    fetch(url)
      .then(response => {
        if (response.ok) {
          return response.json()
        } else {
          this.onDownloadError(`Get Contracts; Code ${response.status} (${response.statusText})`)
        }
      })
      .then(data => {
        console.log(data);
        if (Array.isArray(data)) {
          this.setState({ contracts: data })
        }
      })
      .catch(this.onDownloadError);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      data: {
        ...this.state.data,
        [name]: value
      }
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.onFormSubmit(this.state.data);
    this.setState(this.initialState);
  }

  onSubmitSuccess(response) {
    if (response.ok) {
      this.toastManager.add("Requisition successfully created", {
        appearance: 'success',
        autoDismiss: true,
      });
      // Do something on success
    } else {
      this.onSubmitError(`Code ${response.status} (${response.statusText})`);
    }
    
    console.log(response);
  }

  onSubmitError(error) {
    this.toastManager.add(`Error while sending request: ${error}`, {
      appearance: 'error',
      autoDismiss: true,
    });
  }

  onDownloadError(error) {
    //this.toastManager.add(`Error while retrieving resource: ${error}`, {
    //  appearance: 'error',
    //  autoDismiss: true,
    //});
  }

  handleApiSubmission(values, actions) {
    const url = 'http://localhost:5000/api/accounting/requisitions'
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(values)
    };
    fetch(url, requestOptions)
        .then(this.onSubmitSuccess)
        .catch(this.onSubmitError)
  }

  renderEditable = cellInfo => {
    return (
      <div
        style={{ backgroundColor: "#fafafa" }}
        contentEditable
        suppressContentEditableWarning
        onBlur={e => {
          const data = [...this.state.data];
          data[cellInfo.index][cellInfo.column.id] = e.target.innerHTML;
          this.setState({data: { data }});
        }}
        dangerouslySetInnerHTML={{
          __html: this.state.data[cellInfo.index][cellInfo.column.id]
        }}
      />
    );
  };

  ChangeFormGroupTabPriceTitle(id, data, tagAmount='price', tagQuantity='quantity') {
    var cmuPrice = 0.0;
    
    // Iterate over MatrixEditor items
    if (isArray(data)) {
      for (let index = 0; index < data.length; index++) {
        const item = data[index];
        const itemPrice = (Number(item[tagAmount]) * Number(tagQuantity === null ? 1 : item[tagQuantity]));
        if (!isNaN(itemPrice)) {
          cmuPrice += itemPrice
        }
      }
    }

    this.setState({
      formGroupTabTitles: {
        ...this.state.formGroupTabTitles,
        [id]: cmuPrice.toLocaleString('en-US', { minimumFractionDigits: 2, maximumFractionDigits: 2 })
      }
    });
  }

  GetTabPriceTitle(id, prefix) {
    return `${prefix}${this.state.formGroupTabTitles[id] ? ` ($${this.state.formGroupTabTitles[id]})` : ' ($0.00)'}`
  }

  viewFSCReference() {
    window.open('/fscref.pdf','_blank','height=800,width=600');
  }

  render() {

    let pageTitle;
    if(this.state.data.id) {
      pageTitle = "Edit Requisition"
    } else {
      pageTitle = "Add Requisition"
    }

    return (
      <MainLayout title={pageTitle}>
        <Formik
          initialValues={{
            id: null,
            requester: null,
            purchase_order: [],
            vendor: null,
            receive_by: '2020-01-01',
            subsidiary: null,
            job_code: null,
            task_number: null,
            location: 1,
            delivery_speed: null
          }}
          /*onSubmit={(values, actions) => {
            setTimeout(() => {
              alert(JSON.stringify(values, null, 2));
              actions.setSubmitting(false);
            }, 1000);
          }}*/
          onSubmit={this.handleApiSubmission}
        >
          {(props: FormikProps<any>) => (
            <FormikForm className="BuilderForm-main">
              <Toolbar>
                <Dropdown size="sm" as={ButtonGroup}>
                  <Button type="submit" variant="primary">Save</Button>

                  <Dropdown.Toggle split variant="primary" id="dropdown-split-basic" />

                  <Dropdown.Menu>
                    <Dropdown.Item href="#/action-1">Save</Dropdown.Item>
                    <Dropdown.Item href="#/action-2">Save and New</Dropdown.Item>
                    <Dropdown.Item href="#/action-3">Save and Copy</Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
                <Button type="submit" variant="info" size="sm">Save Draft</Button>
                <Link to="/Requisitions/">
                  <Button variant="secondary" size="sm">Cancel</Button>
                </Link>
                <Button variant="secondary" size="sm">Reset</Button>
                <ToolbarSeparator />
                <Button variant="secondary" size="sm">Import...</Button>
              </Toolbar>

              <FormGroupCard title="Primary Information">

                <Row>
                  <FormDialogListInput
                    name="requester"
                    label="Requester"
                    title="Requester"
                    value={this.state.data.requester}
                    columns={this.Requisitions_dlgListInput_Requester}
                    ajax="http://localhost:5000/api/general/users"
                    onChange={this.handleInputChange}
                    setDisplay={(row) => { return `${row.id} - ${row.first_name} ${row.last_name}` }} />
                  <FormDialogListInput
                    name="vendor"
                    label="Vendor"
                    title="Vendor"
                    value={this.state.data.vendor}
                    columns={this.Requisitions_dlgListInput_Vendor}
                    ajax="http://localhost:5000/api/general/vendors"
                    onChange={this.handleInputChange}
                    setDisplay={(row) => { return `${row.id} - ${row.name}`}} />
                </Row>

                <Row>
                  <FormText
                    data_id="purchase_order"
                    name="PO#" />
                  <FormInput
                    name="receive_by"
                    type="date"
                    label="Receive By"
                    value={this.state.data.receive_by} />
                </Row>

              </FormGroupCard>

              <FormGroupCard title="Classification">

                <Row>
                  <FormSelect name="subsidiary" label="Subsidiary">
                    <option value="1">Exquadrum, Inc.</option>
                  </FormSelect>
                  <FormSelect
                    name="job_code"
                    label="Job Code"
                    type="text"
                    onChange={(event) => {
                      const selectedIndex = event.target.options.selectedIndex;
                      const selectedKey = event.target.options[selectedIndex].getAttribute('data-key');
                      this.setState({contractTasks: selectedKey ? this.state.contracts[selectedKey].tasks : []});
                    }}
                  >
                    <option>---</option>
                    {this.state.contracts.map((contract, index) =>
                      <option key={index} data-key={index} value={contract.id}>{ `${contract.job_code} (${contract.name})` }</option>
                    )}
                  </FormSelect>
                  <FormSelect name="task_number" label="Task Number" type="text">
                    <option>---</option>
                    {this.state.contractTasks.map((task, index) =>
                      <option key={index} value={task.id}>{ `${task.number} (${task.name})` }</option>
                    )}
                  </FormSelect>
                </Row>

                <Row>
                  <FormSelect 
                    name="location"
                    label="Location">
                    <option value="1">Location 1</option>
                    <option value="2">Location 2</option>
                    <option value="3">Location 3</option>
                  </FormSelect>
                  <FormSelect
                    name="delivery_speed"
                    label="Delivery Speed">
                    <option>No Preference</option>
                    <option value="1">USPS Mail</option>
                    <option value="2">Ground</option>
                    <option value="3">Overnight</option>
                    <option value="4">2 Day</option>
                    <option value="5">3 Day</option>
                    <option value="6">eDelivery</option>
                    <option value="7">Will-Call</option>
                    <option value="8">Local Pickup</option>
                    <option value="9">See Comments</option>
                  </FormSelect>
                </Row>

              </FormGroupCard>

              <TabbedFormGroupCard>

                <FormGroupTab tabKey="Requisitions_tabs_items" title="Lines">
                  <TabbedSubFormGroup>
                    <FormGroupTab
                      tabKey="Requisitions_items_items"
                      title={this.GetTabPriceTitle('items', 'Items')}
                    >
                      <FormikMatrixEditor
                        name="items"
                        columns={this.Requisitions_matrixColumns_items}
                        onChange={(e, data) => this.ChangeFormGroupTabPriceTitle('items', data)}
                        onRemoveRow={(data) => this.ChangeFormGroupTabPriceTitle('items', data)}
                        toolbarAppend={
                          <>
                            <ToolbarSeparator />
                            <Button as="a" variant="light" size="sm" onClick={() => this.viewFSCReference()}>View Federal Supply Code Reference</Button>
                          </>
                        } />
                    </FormGroupTab>
                    <FormGroupTab
                      tabKey="Requisitions_items_expenses"
                      title={this.GetTabPriceTitle('expenses', 'Expenses')}
                    >
                      <FormikMatrixEditor
                        name="expenses"
                        columns={this.Requisitions_matrixColumns_expenses}
                        onChange={(e, data) => this.ChangeFormGroupTabPriceTitle('expenses', data, 'amount', null)}
                        onRemoveRow={(data) => this.ChangeFormGroupTabPriceTitle('expenses', data, 'amount', null)} />
                    </FormGroupTab>
                  </TabbedSubFormGroup>
                </FormGroupTab>

                <FormGroupTab tabKey="Requisitions_tabs_billing" title="Billing">
                  <FormGroupCard title="Billing Address">
                    <Row>
                      <FormSelect name="billing_address" label="Vendor Select">
                      </FormSelect>
                    </Row>
                  </FormGroupCard>
                  <Row>
                    <FormSelect name="terms" label="Terms">
                      <option></option>
                      <option value="1">Due on receipt</option>
                      <option value="2">1%/10 Net 30</option>
                      <option value="3">2%/10 Net 30</option>
                      <option value="4">Net 15</option>
                      <option value="5">Net 30</option>
                      <option value="6">Net 60</option>
                    </FormSelect>
                  </Row>
                </FormGroupTab>

                <FormGroupTab tabKey="Requisitions_tabs_relationships" title="Relationships">
                  <TabbedSubFormGroup>
                    <FormGroupTab
                      tabKey="Requisitions_relationships_contacts"
                      title="Contacts"
                    >
                      <FormikMatrixEditor
                        name="contacts"
                        columns={this.Requisitions_matrixColumns_contacts} />
                    </FormGroupTab>
                  </TabbedSubFormGroup>
                </FormGroupTab>
                <FormGroupTab tabKey="Requisitions_tabs_communication" title="Communication">
                  <TabbedSubFormGroup>
                    <FormGroupTab
                      tabKey="Requisitions_communication_messages"
                      title="Messages"
                    >
                    </FormGroupTab>
                    <FormGroupTab
                      tabKey="Requisitions_communication_events"
                      title="Events"
                    >
                    </FormGroupTab>
                    <FormGroupTab
                      tabKey="Requisitions_communication_tasks"
                      title="Tasks"
                    >
                    </FormGroupTab>
                    <FormGroupTab
                      tabKey="Requisitions_communication_phone_calls"
                      title="Phone Calls"
                    >
                    </FormGroupTab>
                    <FormGroupTab
                      tabKey="Requisitions_communication_files"
                      title="Files"
                    >
                    </FormGroupTab>
                    <FormGroupTab
                      tabKey="Requisitions_communication_user_notes"
                      title="User Notes"
                    >
                    </FormGroupTab>
                  </TabbedSubFormGroup>
                </FormGroupTab>
              </TabbedFormGroupCard>
            </FormikForm>
          )}
        </Formik>
      </MainLayout>
    );
  }
}

export default withToastManager(AddRequisition);