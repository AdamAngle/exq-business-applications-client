import React, { Component } from "react";
import { Link } from "react-router-dom";
// eslint-disable-next-line
import { Row, Form, Col, Button, ButtonGroup, Dropdown } from 'react-bootstrap';
import MainLayout from 'components/Pages/MainLayout.js';
import Toolbar, { ToolbarSeparator } from 'components/Layout/Toolbar/Toolbar.js';
import Table from 'components/Layout/DataTable/DataTable.js';
import { withToastManager } from "react-toast-notifications";
 
class RequisitionsHome extends Component {
  constructor(props) {
    super(props);
    this.defaultColumns = [
      {
        title: 'New',
        orderable: false,
        data: function ( row, type, val, meta ) {
          return '+';
        }
      },
      {
        title: 'Edit | View',
        orderable: false,
        data: function ( row, type, val, meta ) {
          return 'Edit | View';
        }
      },
      {
        title: 'Date',
        data: 'date'
      },
      {
        title: 'Number',
        data: 'number'
      },
      {
        title: 'Job',
        data: 'task'
      },
      {
        title: 'Vendor',
        data: 'vendor'
      },
      {
        title: 'Short Desc.',
        data: 'vendor'
      },
      {
        title: 'Status',
        data: 'vendor'
      },
      {
        title: 'Amount',
        data: 'vendor'
      }
    ]

    if(props.product){
      this.state = props.product
    } else {
      this.state = this.initialState;
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.onFormSubmit(this.state);
    this.setState(this.initialState);
  }

  render() {
    return (
      <MainLayout title="Requisitions">
        <Form onSubmit={this.handleSubmit} className="TableForm-main">
          <Toolbar>
            <Form.Group controlId="table_View">
              <Form.Label>
                View
              </Form.Label>
              <Form.Control as="select" size="sm">
                <option>Default View</option>
              </Form.Control>
            </Form.Group>
            <ToolbarSeparator />
            <Link to="/Requisitions/New/">
              <Button variant="primary" size="sm">New Transaction</Button>
            </Link>
          </Toolbar>
        </Form>
        <Table columns={this.defaultColumns} ajax="http://localhost:5000/api/accounting/requisitions" />
      </MainLayout>
    );
  }
}

export {RequisitionsHome};
export default withToastManager(RequisitionsHome);