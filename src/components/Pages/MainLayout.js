import React, { Component } from "react";

class PageLayout extends Component {
  render() {
    return (
      <div className="container-fluid PageLayout-main">
        <h5>{this.props.title}</h5>
        {this.props.children}
      </div>
    );
  }
}

export {PageLayout};
export default PageLayout;