import React from 'react';
import { Header, SubHeader } from './components/Layout/Header/Header.js'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import {
  Route,
  BrowserRouter
} from "react-router-dom";
import './App.css';

// Import pages
import { Home } from './components/Pages/Home/Home.js'
import { RecentActivity } from './components/Pages/RecentActivity/RecentActivity.js'
import { RequisitionsHome } from './components/Pages/Requisitions/RequisitionsHome.js'
import AddRequisition from './components/Pages/Requisitions/AddRequisition.js';
import { ToastProvider } from 'react-toast-notifications';

require('bootstrap')

library.add(fas, far)

function App() {
  return (
    <ToastProvider placement="bottom-right">
      <div className="App">
        <BrowserRouter>
          <Header />
          <SubHeader />

          <div className="content">
            <Route exact path={[ "/" ]} component={Home} />
            <Route path="/History/" component={RecentActivity} />
            <Route exact path={["/Requisitions/", "/Requisitions/Home/"]} component={RequisitionsHome} />
            <Route exact path={[ "/Requisitions/New/" ]} component={AddRequisition} />
          </div>
        </BrowserRouter>
      </div>
    </ToastProvider>
  );
}

export default App;
